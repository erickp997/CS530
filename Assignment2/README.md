# CS530 Assignment 2
## General Description
A set of programs performing that generate K pseudoprime numbers using the Miller-Rabin method. The program reads in
number K for how many pseudoprime numbers will be generated and number B represents the bit length. The pseudoprime
numbers should be at least bit length 2^B. It starts four threads that will run generatePrimes and the main thread will collect
the pseudoprimes from the threads with the help of the linked list until all K pseudoprime numbers are generated.

Erick Pulido

erickpulido97@gmail.com

## NOTES FOR SCOTT - PLEASE READ
* My first assignment for this class mentioned there was an update recently but I hadn't touched anything on that assignment since
* it was submitted. If you click on the actual folder for cs530assignment1, it shows I haven't actually modified anything since
* I submitted it by the deadline. I'm writing this here because I didn't want to modify anything there and get points deducted for 'lateness'.
* The program does produce pseudoprime numbers for B = 1024, however it is a bit slow so if you test that please be patient.
* Also my git is being weird and won't let me push my projects onto gitlab so I had to work around that to get them on gitlab.

## Description of Programs per Assignment Document
### prog2_1.h

> The specific functions for a thread safe linked list. Indicates what prog2_1.c should contain and implement

### prog2_1.c

> The actual thread safe linked list implementation. 

### prog2_2.c

> The pseudoprime number generator consisting of a getNextNum and generatePrimes method along with a main method.The 
main method takes in two inputs K, the amount of pseudoprime numbers to generate and B the bitlength. 

## Example compilations, executions, and outputs
### prog2_1.c - (Do not execute, just compile)
`gcc prog2_1.c -lgmp -pthread`

### prog2_2.c
`gcc prog2_2.c -lgmp -pthread`

`./a.out 4 128`

```
Assignment #2-2, Erick Pulido, erickpulido97@gmail.com
340282366920938463463374607431768211507
340282366920938463463374607431768211537
340282366920938463463374607431768211621
340282366920938463463374607431768211729

```
