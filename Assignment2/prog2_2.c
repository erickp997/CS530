#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <gmp.h>
#include "prog2_1.c"

mpz_t globalCounter;
pthread_mutex_t counterguard = PTHREAD_MUTEX_INITIALIZER;
TSAFELIST *numberList;
int size;

/* Generate next number to check in safe thread manner. */
void getNextNum(mpz_t num) {
    pthread_mutex_lock(&counterguard);
    mpz_set(num, globalCounter);
    mpz_add_ui(globalCounter, globalCounter, 1); // globalCounter++
    pthread_mutex_unlock(&counterguard);
    return;   
}

/* Generate primes with Miller-Rabin method.
   If the number is pseudoprime, enqueue*/
void *generatePrimes(void *arg) {
    while(1) {
        mpz_t count;
        mpz_init(count);
        getNextNum(count);     
        int tmp = mpz_probab_prime_p(count, 100000);
        if(tmp > 0) {
            pthread_mutex_lock(&counterguard);
            if(size > 0) {
                size--;
                tSafeEnqueue(numberList, count);
            }
            else break;
            pthread_mutex_unlock(&counterguard);
        }
        else continue;
   }
    pthread_mutex_unlock(&counterguard);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    printf("Assignment #2-2, Erick Pulido, erickpulido97@gmail.com\n");

    int K = atoi(argv[1]);
    int B = atoi(argv[2]);
    size = K;
    
    mpz_init(globalCounter);
    numberList = tSafeConstruct();
    mpz_ui_pow_ui(globalCounter, 2, B);

    pthread_t thread1, thread2, thread3, thread4;
    pthread_create(&thread1, NULL, generatePrimes, NULL);
    pthread_create(&thread2, NULL, generatePrimes, NULL);
    pthread_create(&thread3, NULL, generatePrimes, NULL);
    pthread_create(&thread4, NULL, generatePrimes, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);

    return 0;
}
