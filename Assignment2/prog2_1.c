#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <gmp.h>
#include "prog2_1.h"

/* Allocate memory for a TSAFELIST on the heap. */
TSAFELIST* tSafeConstruct() {
    TSAFELIST *tSafeList = malloc(sizeof(TSAFELIST));
    tSafeList->mutex = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(tSafeList->mutex, NULL);
    tSafeList->head = NULL;
    return tSafeList;
}

/* Free associated memory. */
void tSafeDestruct(TSAFELIST *tSafeList) {
    free(tSafeList);
}

/* Add element to the end of the list. */
void tSafeEnqueue(TSAFELIST *tSafeList, mpz_t item) {
    pthread_mutex_lock(tSafeList->mutex);
    if(tSafeList->head == NULL) { // Insertion to empty list
        TSAFENODE *newNode = (TSAFENODE *) malloc(sizeof(TSAFENODE));
        mpz_init(newNode->number);
        tSafeList->head = newNode;
        mpz_set(newNode->number, item);
        newNode->next = NULL;
        gmp_printf("%Zd\n", newNode->number);
    }
    else { // insert to existing list
        TSAFENODE *newNode = (TSAFENODE *)malloc(sizeof(TSAFENODE));
        mpz_init(newNode->number);
        TSAFENODE *current= tSafeList->head;
        while(current->next != NULL) {
            current = current->next;
        }
        current->next = newNode;
        mpz_set(newNode->number, item);
        newNode->next = NULL;
        gmp_printf("%Zd\n", newNode->number);
    }
    pthread_mutex_unlock(tSafeList->mutex);
}

/* Remove element from front of the list and return it.
   If there is no element in the front of the list
   return the TSAFELIST data with isValid = false.
*/
TSAFEDATA tSafeDequeue(TSAFELIST *tSafeList) {
    pthread_mutex_lock(tSafeList->mutex);
    TSAFEDATA *selected = (TSAFEDATA *) malloc(sizeof(TSAFEDATA));
    if(tSafeList->head == NULL) { // Empty list case
        selected->isValid = 0;
        mpz_init(selected->value);
        mpz_set(selected->value, NULL);
        pthread_mutex_unlock(tSafeList->mutex);
        return *selected;
    }
    else {
        mpz_init(selected->value);
        mpz_set(selected->value, tSafeList->head->number);
        tSafeList->head = tSafeList->head->next;
        selected->isValid = 1;
        pthread_mutex_unlock(tSafeList->mutex);
        return *selected;
    }
}
