# CS530 Assignment 1
## General Description
A set of programs performing CRC32 of specific portions
of ELF FILES. Authored by:

Erick Pulido

erickpulido97@gmail.com

## Description of Programs per Assignment Document
### prog1_1.c

> Create a C program that takes a single command line argument FILE. 
The FILE will be a target elf file that you will read as a binary file in its 
entirety. Your program will then print that files CRC32 checksum as an 8 digit 
hex number. 

### prog1_2.c

> Create a C program that takes a single command line argument FILE. 
The FILE will be a target elf file. Your program will then print the CRC32 
checksum of ONLY the program header table (not the file header or the 
section headers or any sections).

### prog1_3.c

> Create a C program thakes two command line arguments, FILE and 
SECTIONNAME. The FILE will be a target elf file, SECTIONNAME will be a 
string identifying which sections for which your program will produce CRC32 
checksums. If there are multiple sections that match the provided 
SECTIONNAME your program will produce CRC32 checksums for each of 
them.

## Example compilations, executions, and outputs
### prog1_1.c
`gcc prog1_1.c -o prog1_1`

`./prog1_1 target_file`

```
Assignment #1-1, Erick Pulido, erickpulido97@gmail.com
4652481b
```

### prog1_2.c
`gcc prog1_2.c -o prog1_2`

`./prog1_2 target_file`

```
Assignment #1-2, Erick Pulido, erickpulido97@gmail.com
fb93e9b0
```

### prog1_3.c
`gcc prog1_3.c -o prog1_3`

`./prog1_3 target_file .shstrtab`

```
Assignment #1-3, Erick Pulido, erickpulido97@gmail.com
6ecaabeb
```
