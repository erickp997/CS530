#include <stdio.h>
#include <stdlib.h>
/*  Perform a simple CRC-32 calculation given a buffer
    and length of item in the buffer. Returns an 8
    digit hexadecimal value. Cited from hackersdelight.org   
*/
unsigned int getcrc(unsigned char *buff, long length) {
    int i, j;
    unsigned int byte, crc, mask;
    i = 0;
    crc = 0xFFFFFFFF;
    while(length > i) {
        byte = buff[i];
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}
int main(int argc, char *argv[]) {
    printf("Assignment #1-2, Erick Pulido, erickpulido97@gmail.com\n");

    long len, headerTable;
    char *buffer;
    short size, numEntries;
    FILE *fp = fopen(argv[1], "rb");

    fseek(fp, 0x36, SEEK_SET); // get the size of the header table
    fread(&size, sizeof(size), 1, fp);  // record the length of the header

    fseek(fp, 0x38, SEEK_SET); // get the number of entries in the table
    fread(&numEntries, sizeof(numEntries), 1, fp); // record how many entries

    fseek(fp, 0x20, SEEK_SET); // get offset of start of table
    fread(&headerTable, sizeof(headerTable), 1, fp); 

    fseek(fp, headerTable, SEEK_SET);

    len = size * numEntries; // space of buffer depends on size and entries    
    buffer = (char*)malloc(len); // allocate space according to size of portion
    fread(buffer, len, 1, fp);
    fclose(fp);

    int ans = getcrc(buffer, len);
    printf("%08x\n", ans);
    return 0;
}
