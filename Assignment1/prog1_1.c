#include <stdio.h>
#include <stdlib.h>
/*  Perform a simple CRC-32 calculation given a buffer
    and length of item in the buffer. Returns an 8
    digit hexadecimal value. Cited from hackersdelight.org   
*/
unsigned int getcrc(unsigned char *buff, long length) {
    int i, j;
    unsigned int byte, crc, mask;
    i = 0;
    crc = 0xFFFFFFFF;
    while(length > i) {
        byte = buff[i]; // get the next bit
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--) { // perform for all 8 bits
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}
int main(int argc, char *argv[]) {
    printf("Assignment #1-1, Erick Pulido, erickpulido97@gmail.com\n");

    /* Gather components for crc32 calculation */
    long len;
    char *buffer;
    FILE *fp = fopen(argv[1], "r");

    fseek(fp, 0, SEEK_END); // get to end of the file
    len = ftell(fp);        // record the length of the file
    fseek(fp, 0, SEEK_SET); // go back to beginning of file

    buffer = (char*)malloc(len); // allocate space according to size of file
    fread(buffer, len, 1, fp);
    fclose(fp);

    int ans = getcrc(buffer, len);
    printf("%08x\n", ans);
    return 0;
}
