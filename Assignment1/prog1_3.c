#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*  Perform a simple CRC-32 calculation given a buffer
    and length of item in the buffer. Returns an 8
    digit hexadecimal value. Cited from hackersdelight.org   
*/
unsigned int getcrc(unsigned char *buff, long length) {
    int i, j;
    unsigned int byte, crc, mask;
    i = 0;
    crc = 0xFFFFFFFF;
    while(length > i) {
        byte = buff[i];
        crc = crc ^ byte;
        for(j = 7; j >= 0; j--) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i++;
    }
    return ~crc;
}
int main(int argc, char *argv[]) {
    printf("Assignment #1-3, Erick Pulido, erickpulido97@gmail.com\n");
    
    FILE *fp = fopen(argv[1], "rb");
    char buffer[100];
    unsigned char sh_name;
    long e_shoff, sectionNames, shOffset, startOfName, nameOffset;
    short e_shentsize, e_shnum, e_shstrndx; 

    fseek(fp, 0x28, SEEK_SET);
    fread(&e_shoff, sizeof(e_shoff), 1, fp);

    fseek(fp, 0x3A, SEEK_SET);
    fread(&e_shentsize, sizeof(e_shentsize), 1, fp);

    fseek(fp, 0x3C, SEEK_SET);
    fread(&e_shnum, sizeof(e_shnum), 1, fp);

    fseek(fp, 0x3E, SEEK_SET);
    fread(&e_shstrndx, sizeof(e_shstrndx), 1, fp);
    
    // temporary variable to find offset for table of string names
    shOffset = e_shoff + (e_shstrndx * e_shentsize) + 0x18;

    fseek(fp, shOffset, SEEK_SET);
    fread(&sectionNames, sizeof(sectionNames), 1, fp);
    
    int i = 0;
    while(i < e_shnum) {
        startOfName = e_shoff + i * e_shentsize;
        // get the start of the name to look through
        fseek(fp, startOfName, SEEK_SET);
        fread(&sh_name, 1, 1, fp);

        nameOffset = sectionNames + sh_name;
        // hold the name in buffer in order to test if it as match
        fseek(fp, nameOffset, SEEK_SET);
        fread(&buffer, 1, 100, fp);

        // print the crc of the name that matches from user input
        if(strcmp(buffer, argv[2]) == 0) {
            unsigned char selected[e_shentsize];
            fseek(fp, startOfName, SEEK_SET);
            fread(&selected, 1, e_shentsize, fp);
            int ans = getcrc(selected, e_shentsize);
            printf("%08x\n", ans);
        }
        i++;
    }
    fclose(fp);
    return 0;
}
